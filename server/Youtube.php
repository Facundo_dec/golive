<?php
  if ( ! ini_get('date.timezone') )
	
  {
    date_default_timezone_set('America/Argentina/Buenos_Aires');
	}

class Youtube{
  
  CONST API_URL = 'https://www.googleapis.com/youtube/v3/';
  CONST API_KEY = 'AIzaSyB3-TUxMMEnrkE2yqSgRK2ijk2bW25kRpY';
  


  /**
  * Make request to the Youtube API
  *
  * @param string $resource // channels, videos, playlists ...
  * @param array $params // Parameters
  *
  * @return array // Result
  */
  public function request($resource, $params){
    // Construction de l'url
    $query_url = self::API_URL.$resource.'/?key='.self::API_KEY.'&'.http_build_query($params);

    // Exécution de la requête
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $query_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }

  /**
  * Get channel informations from a channel ID
  *
  * @param string $channel_id // Channel ID
  *
  * @return array // Channel informations
  */
  public function getChannelInfosById($channel_id){
    $params = [
      'part' => 'id,snippet,contentDetails,statistics,brandingSettings',
      'id' => $channel_id,
      'maxResults' => 1
    ];

    $result = $this->request('channels', $params);
    $json = json_decode($result);

    $item = $json->items[0];

    $channel_infos = [
      'id'                => $item->id,
      'title'             => $item->snippet->title,
      'description'       => nl2br($item->snippet->description),
      'inscription'       => $item->snippet->publishedAt,
      'avatar'            => $item->snippet->thumbnails->high->url,
      'playlistId'        => $item->contentDetails->relatedPlaylists->uploads,
      'viewCount'         => $item->statistics->viewCount,
      'subscriberCount'   => $item->statistics->subscriberCount,
      'videoCount'        => $item->statistics->videoCount,
      'color'             => $item->brandingSettings->channel->profileColor,
      'bannerImage'       => $item->brandingSettings->image->bannerTabletExtraHdImageUrl,
      'backgroundImage'   => $item->brandingSettings->image->bannerTvImageUrl
    ];
    return $channel_infos;
  }

  /**
  * Get channel username from an ID
  *
  * @param string $channel_id // Name of Youtube channel
  *
  * @return string // Channel ID
  */
  public function getChannelIdByUsername($channel_username){
    $params = [
      'part' => 'id',
      'forUsername' => $channel_username,
      'maxResults' => 1
    ];

    $result = $this->request('channels', $params);
    $json = json_decode($result);
    return (isset($json->items[0]->id)) ? $json->items[0]->id : false;
  }

  /**
  * Get channel informations from a Youtube channel URL
  *
  * @param string $channel_id // Channel URL
  *
  * @return array // Channel informations
  */
  public function getChannelInfosByUrl($channel_url){

    preg_match('#https?://www\.youtube\.com/(channel|user)/(.+)#', $channel_url, $match);

    if(isset($match[2])){
      $channel_id = $match[2];
    }
    else{
      return false;
    }

    $result = $this->getChannelIdByUsername($channel_id);
    if($result){
      $channel_id = $result;
    }
    return $this->getChannelInfosById($channel_id);
  }

  /**
  * Get videos informations
  *
  * @param array $video_ids // Videos ids
  *
  * @return array // Videos informations
  */
  public function getVideos($videos_ids){

    $params = [
      'part' => 'id,snippet,contentDetails,statistics,player',
      'id' => join(',', $videos_ids),
      'maxResults' => 1
    ];

    $result = $this->request('videos', $params);

    $json = json_decode($result);

    usort($json->items, function($a, $b) {
        return strtotime($b->snippet->publishedAt) - strtotime($a->snippet->publishedAt);
    });

    for($i=0 ; $i<count($json->items) ; $i++){

      $item = $json->items[$i];

      $date = $this->timeAgo($item->snippet->publishedAt);
      $views = number_format($item->statistics->viewCount, 0, '.', ' ');
      if(isset($item->statistics->likeCount)){
        $likes = number_format($item->statistics->likeCount, 0, '.', ' ');
      }
      else{
        $likes = 0;
      }

      if(isset($item->statistics->dislikeCount)){
        $dislikes = number_format($item->statistics->dislikeCount, 0, '.', ' ');
      }
      else{
        $dislikes = 0;
      }

      if(isset($item->statistics->commentCount)){
        $comments = $item->statistics->commentCount;
      }
      else{
        $comments = 0;
      }

      $description = nl2br($item->snippet->description);
      $description = preg_replace('#(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])#i', '<a href="$0" target="_blank" title="$0">$0</a>', $description);

      $videos_infos[$item->id] = [
        'id'              => $item->id,
        'title'           => $item->snippet->title,
        'date'            => $date,
        'description'     => $description,
        'thumbnail'       => $item->snippet->thumbnails->high->url,
        'username'        => $item->snippet->channelTitle,
        'duration'        => $this->duration($item->contentDetails->duration),
        'views'           => $views,
        'likes'           => $likes,
        'dislikes'        => $dislikes,
        'comments'        => $comments
      ];
    }
    return $videos_infos;
  }

  /**
  * Get videos informations from a playlist ID
  *
  * @param string $playlist_id // Playlist ID
  * @param int $nb_results // Number of videos to retrieve
  * @param string $page_token // Token of next page, leave empty if it's the first page
  *
  * @return array // Videos informations + nextPageToken
  */
  public function getVideosByPlaylistId($playlist_id, $nb_results, $page_token){

    $params = [
      'part' => 'snippet',
      'playlistId' => $playlist_id,
      'maxResults' => $nb_results,
      'pageToken' => $page_token
    ];

    $result = $this->request('playlistItems', $params);
    $json = json_decode($result);

    isset($json->nextPageToken) ? $nextPageToken = $json->nextPageToken : $nextPageToken = 'end';

    for($i=0 ; $i<count($json->items) ; $i++){
      $videos_ids[] = $json->items[$i]->snippet->resourceId->videoId;
    }

    return ['nextPageToken' => $nextPageToken, 'videos' => $this->getVideos($videos_ids)];
  }

  /**
  * Get 50 Top com from a video ID
  *
  * @param string $video_id // Video ID
  * @param int $nb_comments // Nomber of comments to retrieve
  * @param string $page_token // Token of the next page, leave empty if it's the first page
  *
  * @return array // Comments informations
  */
  public function getComments($video_id, $nb_comments, $page_token){

    $params = [
      'part' => 'snippet',
      'videoId' => $video_id,
      'maxResults' => $nb_comments,
      'order' => 'relevance',
      'textFormat' => 'plainText',
      'pageToken' => $page_token
    ];

    $result = $this->request('commentThreads', $params);
    $json = json_decode($result);

    isset($json->nextPageToken) ? $nextPageToken = $json->nextPageToken : $nextPageToken = 'end';

    for($i=0 ; $i<count($json->items) ; $i++){
      $item = $json->items[$i]->snippet->topLevelComment;
      $date = $this->timeAgo($item->snippet->publishedAt);
      $comments[] = [
        'username' => $item->snippet->authorDisplayName,
        'comment' => $item->snippet->textDisplay,
        'avatar' => $item->snippet->authorProfileImageUrl,
        'likes' => $item->snippet->likeCount,
        'date' => $date
      ];
    }

    if(count($json->items) == 0){
      $comments = '';
    }

    return ['nextPageToken' => $nextPageToken, 'comments' => $comments];
  }

  /**
  * Get playlists from a Youtube channel
  *
  * @param string $channel_id // Channel ID
  *
  * @return array // Playlists informations
  */
  public function getPlaylists($channel_id){

    $params = [
      'part' => 'snippet,contentDetails,status',
      'channelId' => $channel_id,
      'maxResults' => 50
    ];

    $result = $this->request('playlists', $params);
    $json = json_decode($result);

    if(count($json->items) == 0){
      $playlists_infos = [];
    }
    else{
      for($i = 0 ; $i < count($json->items) ; $i++){
          $playlists_infos[$json->items[$i]->id] = [
            'id'              => $json->items[$i]->id,
            'title'           => $json->items[$i]->snippet->title,
            'thumbnail'       => $json->items[$i]->snippet->thumbnails->high->url,
            'count'           => $json->items[$i]->contentDetails->itemCount
          ];
      }
    }

    return $playlists_infos;
  }

  /**
  * Convert a Youtube date to an elasped time
  *
  * @param string $datetime // Date in format : '2016-01-25-T17:45:57.000Z'
  *
  * @return string // Date in format '2 weeks ago'
  */
  private function timeAgo($datetime) {
    $datetime = preg_replace('#([0-9]{4})-([0-9]{2})-([0-9]{2})-T([0-9]{2}):([0-9]{2}):([0-9]{2})\.000Z#', '$1-$2-$3 $4:$5:$6', $datetime);

    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string).' ago' : 'just now';
  }

  /**
  * Convert Youtube duration to mm:ss format
  *
  * @param string $duration // Duration in format PT12M54S
  *
  * @return string // Duration in format mm:ss
  */
  private function duration($duration) {
    if(preg_match('#H#', $duration)){
      preg_match('#PT([0-9]{1,2})H([0-9]{1,2})M([0-9]{1,2})S#', $duration, $matches);
      isset($matches[1]) ? $h = $matches[1] : $h = '00';
      isset($matches[2]) ? $m = $matches[2] : $m = '00';
      isset($matches[3]) ? $s = $matches[2] : $s = '00';
      (strlen($m) == 1) ? $m = '0'.$m : $m = $m;
      (strlen($s) == 1) ? $s = '0'.$s : $s = $s;
      $time = $h.':'.$m.':'.$s;
    }
    elseif(preg_match('#M#', $duration)){
      preg_match('#PT([0-9]{1,2})M([0-9]{1,2})S#', $duration, $matches);
      isset($matches[1]) ? $m = $matches[1] : $m = '00';
      isset($matches[2]) ? $s = $matches[2] : $s = '00';
      (strlen($s) == 1) ? $s = '0'.$s : $s = $s;
      $time = $m.':'.$s;
    }
    elseif(preg_match('#S#', $duration)){
      preg_match('#PT([0-9]{1,2})S#', $duration, $matches);
      (strlen($matches[1]) == 1) ? $s = '0'.$matches[1] : $s = $matches[1];
      $time = '00:'.$s;
    }
    else{
      $time = '00:00';
    }

    return $time;
  }
}
