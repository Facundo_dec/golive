<?php

  // Your mail, for the contact form
  $Email = 'facundo@cuerti.com';

  // Channel information
  $ChannelID  = 'UCzuqhhs6NWbgTzMuM09WKDQ';
  $PlaylistID = 'PLU8wpH_LfhmsSVRA8bSknO4-2wXvYXS4C';

  // Your website or Youtube page, for the about page
  $WebsiteURL = 'http://www.cuerti.com';

  // Your social networks for the about page, leave blank to those you have not
  $SocialNetworks = array(
    'android-globe'     => 'http://www.google.com', // Website
    'social-twitter'    => 'http://www.twitter.com', // Twitter
    'social-facebook'   => 'http://www.facebook.com', // Facebook
    'social-instagram'  => 'http://www.instagram.com', // Instagram
    'social-googleplus' => 'http://www.google.com', // Google Plus
    'social-twitch'     => '', // Twitch
    'social-snapchat'   => '' // Snapchat
  );
