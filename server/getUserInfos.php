<?php

  header("Access-Control-Allow-Origin: *");

  require_once('Youtube.php');
  require_once('config.php');

  $Youtube = new Youtube();

  function formatNumber($input){
    $suffixes = array('', 'k', 'm', 'b', 't');
    $suffixIndex = 0;

    while(abs($input) >= 1000 && $suffixIndex < sizeof($suffixes)){
        $suffixIndex++;
        $input /= 1000;
    }
    $input > 0 ? $nb = floor($input * 1000) / 1000 : $nb = ceil($input * 1000) / 1000;
    $nb = substr($nb, 0, 3);
    if(substr($nb, -1, 1) == '.'){
      $nb = preg_replace('#\.#', '', $nb);
    }
    return $nb.$suffixes[$suffixIndex];
  }

  $userInfos = $Youtube->getChannelInfosById($ChannelID);

  $userInfos['viewCount'] = formatNumber($userInfos['viewCount']);
  $userInfos['subscriberCount'] = formatNumber($userInfos['subscriberCount']);
  $userInfos['videoCount'] = formatNumber($userInfos['videoCount']);

  $userInfos['website'] = $WebsiteURL;

  foreach($SocialNetworks as $key => $val ) {
    if($val != '') {
        $userInfos['social'][$key] = $val;
    }
  }

  $userInfos['playlists'] = $Youtube->getPlaylists($ChannelID);

  echo json_encode($userInfos);
