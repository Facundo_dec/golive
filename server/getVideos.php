<?php

header("Access-Control-Allow-Origin: *");

require_once('Youtube.php');
require_once('config.php');

$Youtube = new Youtube();

if(isset($_GET['playlistid']) && $_GET['playlistid'] != ''){
    $PlaylistID = $_GET['playlistid'];
}

isset($_GET['token']) ? $nextPageToken = $_GET['token'] : $nextPageToken = '';
echo json_encode($Youtube->getVideosByPlaylistId($PlaylistID, 10, $nextPageToken));
