<?php

  header("Access-Control-Allow-Origin: *");
  require_once('Youtube.php');
  $Youtube = new Youtube();

  $videoID = $_GET['videoID'];

  isset($_GET['token']) ? $nextPageToken = $_GET['token'] : $nextPageToken = '';

  echo json_encode($Youtube->getComments($videoID, 20, $nextPageToken));
