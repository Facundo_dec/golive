(function () {

  "use strict";

  var serverURL = '/br/golive/server/';

  // Return the number of elements of an object
  function objectLength(obj) {
    var result = 0;
    for(var prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        result++;
      }
    }
    return result;
  }

  // Return fusion of two objects
  function extend(a, b){
    for(var key in b)
        if(b.hasOwnProperty(key))
            a[key] = b[key];
    return a;
  }

  // Preload all of the images, and display the videos in the DOM
  function preload(videos){
    var posts_to_load = objectLength(videos);
    var posts_loaded = 0;

    $$.each(videos, function(index, video){
      var thumbnail = new Image();
      thumbnail.onload = function () {
        posts_loaded = posts_loaded + 1;
        if(posts_loaded == posts_to_load){
          displayVideos(videos);
        }
      };
      thumbnail.src = video.thumbnail;
    });
  }

  // Display the videos in the DOM
  function displayVideos(videos){
    $$.each(videos, function(index, video) {
      $$('#post-list').append(
        '<li>'+
          '<a href="video.html?idvideo='+ video.id +'">'+
            '<div class="post-thumbnail"><img src="'+video.thumbnail+'" alt=""></div>'+
            '<div class="post-infos">'+
              '<div class="post-title">'+
                '<span>'+video.title+'</span>'+
              '</div>'+
              '<div class="post-views"><i class="icon-demo icon-eye"></i>'+ video.views + ' views</div>'+
              '<div class="post-date"><i class="ion-android-time"></i>'+video.date+'</div>'+
            '</div>'+
          '</a>'+
        '</li>'
      );
    });
  }

  // Display the comments in the DOM
  function displayComments(comments){
    $$.each(comments, function(index, comment){
      $$('#comment-list').append(
        '<li>'+
          '<img class="comment-avatar" src="'+comment.avatar+'" onerror="this.src = \'img/avatar-default.jpg\';">'+
          '<div class="comment-details">'+
            '<span class="comment-author">'+comment.username+'</span>'+
            '<span class="comment-date">'+comment.date+'</span>'+
            '<span class="comment-content">'+comment.comment+'</span>'+
            '<span class="comment-likes"><i class="demo-icon icon-thumbs-up"></i> '+comment.likes+'</span>'+
          '</div>'+
        '</li>'
      );
    });
  }

  // Display a message in the main view when the user is not connected to Internet
  function noConnexion(){
    $$('#post-list').html('<div class="no-connexion"><i class="icon ion-wifi"></i>No Internet Connexion<p>Make sure you are connected to Internet and try to pull-to-refresh by dragging to the bottom.</p></div>');
  }

  // Check if it's a correct email
  function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  var app = new Framework7;
  var $$ = Dom7;
  var viewMain = app.addView('#view-main');
  var pullToRefreshPosts = $$('#content-posts');

  var cachedPosts = [];
  var nextPageToken = '';

  var isLoading = false;
  var isInfiniteScroll = true;

  var currentPlaylistID = '';


  // At the opening of the application, retrieves videos
  $$.ajax({
    url: serverURL + 'getVideos.php',
    dataType: 'json',
    timeout: 8000,
    success: function(result){

      cachedPosts = result.videos;
      nextPageToken = result.nextPageToken;

      preload(result.videos);

      // If they are the last videos
      if(result.nextPageToken == 'end'){
        // Remove the infinite scroll
        app.detachInfiniteScroll($$('#content-posts'));
        $$('#infinite-loader').remove();
        isInfiniteScroll = false;
      }
    },
    error: function(){
      noConnexion();
    }
  });


  // When pullToRefresh is called
  pullToRefreshPosts.on('refresh', function (e) {
    $$.ajax({
      url: serverURL + 'getVideos.php?playlistid=' + currentPlaylistID,
      dataType: 'json',
      timeout: 8000,
      success: function(result){

        // If there are no new videos
        if (typeof myVar != "undefined") {
          if(cachedPosts[Object.keys(cachedPosts)[0]].id == result.videos[Object.keys(result.videos)[0]].id){
            app.pullToRefreshDone(pullToRefreshPosts);
          }
        }
        else{

          cachedPosts = result.videos;
          nextPageToken = result.nextPageToken;

          $$('#post-list').html('');
          preload(result.videos);
          app.pullToRefreshDone(pullToRefreshPosts);

          if(result.nextPageToken == 'end'){
            app.detachInfiniteScroll($$('#content-posts'));
            $$('#infinite-loader').remove();
            isInfiniteScroll = false;
          }
        }
      },
      error: function(){
        noConnexion();
        app.pullToRefreshDone(pullToRefreshPosts);
      }
    });
  });

  // When infiniteScroll is called
  $$('#content-posts').on('infinite', function () {
    // Exit, if infiniteLoadingInterviews in progress
    if (isLoading) return;
    isLoading = true;

    // Retrives next videos with the token
    $$.ajax({
      url: serverURL + 'getVideos.php?playlistid=' + currentPlaylistID + '&token=' + nextPageToken,
      dataType: 'json',
      timeout: 8000,
      success: function(result){

        // Add videos to the local array
        cachedPosts = extend(cachedPosts, result.videos);
        nextPageToken = result.nextPageToken;

        // Display videos
        preload(result.videos);

        if(result.nextPageToken == 'end'){
          app.detachInfiniteScroll($$('#content-posts'));
          $$('#infinite-loader').remove();
          isInfiniteScroll = false;
        }
        isLoading = false;
      },
      error: function(){
        noConnexion();
        isLoading = false;
      }
    });
  });

  var commentsLoading = false;
  var nextPageTokenComments = '';

  // When a post video is openened
  app.onPageInit('video', function(page){

    commentsLoading = true;

    $$('#view-main .back-hidden').toggleClass('back-hidden back-visible');

    // Id of the video
    var videoID = page.query.idvideo;

    // Replace elements on the video page
    $$('#content-video .page-video-title').html(cachedPosts[videoID].title);
    $$('#content-video .page-video-views').html(cachedPosts[videoID].views+' views');
    $$('#content-video .page-video-likes-count').html(cachedPosts[videoID].likes);
    $$('#content-video .page-video-dislikes-count').html(cachedPosts[videoID].dislikes);
    $$('#content-video .page-video-progress').attr({
      max: parseInt(cachedPosts[videoID].likes.replace(/ /g, '')) + parseInt(cachedPosts[videoID].dislikes.replace(/ /g, '')),
      value: parseInt(cachedPosts[videoID].likes.replace(/ /g, '')) + parseInt(cachedPosts[videoID].dislikes.replace(/ /g, '')) - parseInt(cachedPosts[videoID].dislikes.replace(/ /g,""))
    });
    $$('#content-video .page-video-username').html(cachedPosts[videoID].username);
    $$('#content-video .page-video-date').html(cachedPosts[videoID].date);
    $$('#content-video .page-video-description').html(cachedPosts[videoID].description);
    $$('#content-video #youtube-player').attr('src', 'https://www.youtube.com/embed/'+videoID+'?html5=1&autoplay=0&controls=0&showinfo=0&rel=0');
    $$('#content-video .page-video-comments-count').html(cachedPosts[videoID].comments);

    // Retrieves comments
    $$.ajax({
      url: serverURL + 'getComments.php?videoID='+videoID,
      dataType: 'json',
      timeout: 8000,
      success: function(result){
        nextPageTokenComments = result.nextPageToken;
        displayComments(result.comments);
        commentsLoading = false;
        // If they are the last comments
        if(result.nextPageToken == 'end'){
          app.detachInfiniteScroll($$('#content-video'));
          $$('#infinite-loader-comments').remove();
        }
      },
      error: function(){
        commentsLoading = false;
      }
    });

    // When comments infinite scroll is triggered
    $$('#content-video').on('infinite', function () {
      if (commentsLoading) return;
      commentsLoading = true;

      // Retrieves next comments
      $$.ajax({
        url: serverURL + 'getComments.php?videoID=' + videoID + '&token='+nextPageTokenComments,
        dataType: 'json',
        timeout: 8000,
        success: function(result){
          nextPageTokenComments = result.nextPageToken;
          displayComments(result.comments);
          commentsLoading = false;

          if(result.nextPageToken == 'end'){
            app.detachInfiniteScroll($$('#content-video'));
            $$('#infinite-loader-comments').remove();
          }
        },
        error: function(){
          commentsLoading = false;
        }
      });
    });
  });

  // When back arrow is clicked
  app.onPageBack('video', function(e){
    $$('#view-main .back-visible').toggleClass('back-hidden back-visible');
  });

  // At the opening of the app, retrieves user infos (for about page and playlists)
  $$.ajax({
    url: serverURL + 'getUserInfos.php',
    dataType: 'json',
    timeout: 8000,
    success: function(result){

      /****** About page *******/

      $$('#view-about .about-author').html(result.title);
      $$('#view-about .about-website-text').html(result.website);
      $$('#view-about .about-followers .about-count').html(result.subscriberCount);
      $$('#view-about .about-videos .about-count').html(result.videoCount);
      $$('#view-about .about-views .about-count').html(result.viewCount);
      $$('#view-about .about-description').html(result.description);

      var social = result.social;
      for(var network in social){
        var url = social[network];
        $$('#view-about .about-icons').append('<a href="' + url + '" class="external" target="_blank"><i class="icon ion-' + network + '"></i></a>');
      }

      /****** Playlists *******/

      // Add the playlists
      $$('#playlists').append('<a id="'+result.playlistId+'"><li class="all-videos">ALL VIDEOS</a>');
      $$.each(result.playlists, function(index, playlist){
        $$('#playlists').append('<a id="'+playlist.id+'"><li>'+playlist.title + ' <span class="playlist-count">(' + playlist.count + ')</span></li></a>');
      });

      // When a playlist is clicked
      $$('#playlists a').on('click', function(){
        app.closePanel();

        // If we are in the video page, then go back
        if($$('.navbar-inner .left').hasClass('back-visible')){
          viewMain.router.back();
        }

        currentPlaylistID = $$(this).attr('id');

        // If the infinite preloaded was removed previously, then we add it again
        if(!isInfiniteScroll){
          app.attachInfiniteScroll($$('#content-posts'));
          isInfiniteScroll = true;
          $$('#content-posts').append('<span id="infinite-loader" class="preloader"></span>');
        }

        // Retrieves the playlist videos
        $$('#post-list').html('');
        $$.ajax({
          url: serverURL + 'getVideos.php?playlistid=' + $$(this).attr('id'),
          dataType: 'json',
          timeout: 8000,
          success: function(result){

            cachedPosts = result.videos;
            nextPageToken = result.nextPageToken;

            preload(result.videos);

            if(result.nextPageToken == 'end'){
              app.detachInfiniteScroll($$('#content-posts'));
              $$('#infinite-loader').remove();
              isInfiniteScroll = false;
            }
          },
          error: function(){
            console.log("Make sure your are connected to Internet");
          }
        });
      });
    },
    error: function(){
      console.log("Make sure your are connected to Internet");
    }
  });


  // Contact form
  $$('#contact-submit').on('click', function(){
    // Check if one or more fields are empty
    if($$('#contact-name').val() == '' || $$('#contact-email').val() == '' || $$('#contact-subject').val() == '' || $$('#contact-message').val() == ''){
      if($$('#contact-name').val() == ''){
        $$('#contact-name').attr('placeholder', 'Required');
      }
      if($$('#contact-email').val() == ''){
        $$('#contact-email').attr('placeholder', 'Required');
      }
      if($$('#contact-subject').val() == ''){
        $$('#contact-subject').attr('placeholder', 'Required');
      }
      if($$('#contact-message').val() == ''){
        $$('#contact-message').attr('placeholder', 'Required');
      }
    }
    else{
      // Check if it's a correct mail
      if(validateEmail($$('#contact-email').val())){
        // Send the mail
        $$.ajax({
          url: serverURL + 'mail.php?name=' + $$('#contact-name').val() + '&email=' + $$('#contact-mail').val() + '&subject=' + $$('#contact-name').val() + '&message=' + $$('#contact-message').val(),
          dataType: 'text',
          timeout: 8000,
          success: function(result){
            if(result == "success"){
              $$('#contact-name').val('');
              $$('#contact-email').val('');
              $$('#contact-subject').val('');
              $$('#contact-message').val('');

              app.addNotification({
                 title: 'Sucess',
                 message: 'Your message was successfully sent !'
             });
            }
          },
          error: function(){
            console.log("Error when trying to send the mail");
          }
        });
      }
      else{
        $$('#contact-email').attr('placeholder', 'Incorrect mail');
      }
    }
  });

}());
